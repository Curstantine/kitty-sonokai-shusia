# kitty-sonokai-shusia

My remix of [sainnhe](https://github.com/sainnhe)'s [sonokai](https://github.com/sainnhe/sonokai)/shusia theme based from [rsaihe/sonokai-kitty](https://github.com/rsaihe/sonokai-kitty).

## Installation

```
git clone https://gitlab.com/Curstantine/kitty-sonokai-shusia.git ~/.config/kitty/themes/
ln -s ~/.config/kitty/current-theme.conf ~/.config/kitty/themes/kitty-sonokai-shusia/theme.conf
echo "include current-theme.conf" >> ~/.config/kitty/kitty.conf
```

## License

Do whatever you want.

